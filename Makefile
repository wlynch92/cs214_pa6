CFLAGS= -Wall -g

all: bookorder

bookorder: bookorder.o
	gcc $(CFLAGS) -o bookorder bookorder.o -lpthread

bookorder.o: bookorder.c bookorder.h
	gcc $(CFLAGS) -c bookorder.c

clean:
	rm bookorder *.o

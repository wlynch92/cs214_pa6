#include "bookorder.h"

sem_t finish;
User *userdb;

/* This function creates the user database (linked list) 
 * from given the database.txt. Each user contains two 
 * ptrs, one to complete transactions, and one to failed
 * translations.
 */
User* build_userdb(char *userfile) {

    FILE *userinfo = fopen(userfile, "r");
    char line[2048];
    char *buffer;
    User *userdb = NULL,*curr=NULL; /* head of userdb list */

    if (userinfo != NULL) {

        while (fgets(line, 2048, userinfo) != NULL) {

            /* initialize user struct */
            User *u = (User*)malloc(sizeof(User));
            u->name = (char*)malloc(sizeof(char)*256);
            u->custID = 0;
            u->balance = 0.0;
            u->address = (char*)malloc(sizeof(char)*256);
            u->state = (char *)malloc(sizeof(char)*50);
            u->zip = (char*)malloc(sizeof(char)*10);
            u->completed = create_orderq();
            u->failed = create_orderq();
            pthread_mutex_init(&(u->lock),NULL);
            u->next = NULL;

            /* fill in user info */

            /* name */
            buffer = strtok(line, "|\"");
            strcpy(u->name, buffer);

            /* custID */
            buffer = strtok(NULL, " |\"");
            u->custID = atoi(buffer);

            /* balance */
            buffer = strtok(NULL, " |\"");
            u->balance = atof(buffer);

            /* address */
            buffer = strtok(NULL, "|\""); /* space */
            buffer = strtok(NULL, "|\"");
            strcpy(u->address, buffer);

            /* state */
            buffer = strtok(NULL, "|\""); /* space */
            buffer = strtok(NULL, "|\"");
            strcpy(u->state, buffer);

            /* zip code */
            buffer = strtok(NULL, " |\"");
            strcpy(u->zip, buffer);

            /* insert into userdb list */
            if (userdb == NULL){ 
                userdb = u;
                curr=u;
            } else {
                curr->next=u;
                curr=curr->next;
            }
        }
        fclose(userinfo);
        return userdb;
    }
    else {
        fprintf(stderr, "File %s cannot be opened, exiting.\n", userfile);
    }
    return NULL;
}

/* This function should destroy everything in the user and its orders */
void destroy_userdb(User * head){
    if (head==NULL){
        return;
    }
    User *user=head;
    while (user!=NULL){
        free(user->name);
        free(user->address);
        free(user->state);
        free(user->zip);
        destroy_orderq(user->completed);
        destroy_orderq(user->failed);
        User *tmp=user;
        user=user->next;
        free(tmp);
    }
}

/* Initializes the queue for a single category */
OrderQueue* create_orderq(){
    OrderQueue *oq=(OrderQueue *)malloc(sizeof(OrderQueue));
    oq->tail=NULL;
    oq->size=0;
    pthread_mutex_init(&(oq->mlock),NULL);
    sem_init(&(oq->slock),0,0);
    return oq;
}

/* Destroys the given order queue. */
void destroy_orderq(OrderQueue *queue){
    if (queue==NULL){
        return;
    }
    Order *order=dequeue(queue);
    while (order!=NULL){
        destroy_order(order);
    }
    free(queue);
}

void destroy_order(Order *order){
    if (order==NULL){
        return;
    }
    free(order->title);
    free(order->category);
    free(order);
}

/* Builds the order queue for the given category.
 * This method should be a thread.
 */
void *build_orderq(void * arg){ 
    char *orderfile=(char *)arg;
    FILE *fp = fopen(orderfile,"r");
    char *line=NULL;
    size_t size=0;
    int count=0;
    /* 
     * This code is to implement a restricted amount of threads if needed.
     * Commented for now.
     *
     * sem_t numthreads;
     * sem_init(&numthreads,0,3);
     */
    sem_init(&finish,0,0);
    if (fp){
        while (getline(&line,&size,fp)!=-1){
            /* Go through file, build Order */
            Order *order=(Order *)malloc(sizeof(Order));
            char *tok=strtok(line,"\"|\n");
            order->title=(char *)calloc(sizeof(char),strlen(tok)+1);
            strcpy(order->title,tok);
 
            tok=strtok(NULL,"\"| \n");
            order->price=atof(tok);

            tok=strtok(NULL,"\"| \n");
            order->custID=atoi(tok);

            tok=strtok(NULL,"\n\"| ");
            order->category=(char *)calloc(sizeof(char),strlen(tok)+1);
            strcpy(order->category,tok);

            order->next=NULL;

            /* increase count, start consumer thread */
            //sem_wait(&numthreads);
            pthread_t t;
            count++;
            pthread_create(&t,NULL,process_order,order);
            pthread_detach(t);
            //sem_post(&numthreads);
        }
        free(line); 
        fclose(fp);
    } else {
        return NULL;
    }

    /* Wait for all consumer threads to finish before continuing */
    int i;
    for (i=0;i<count;i++){
        sem_wait(&finish);
    }
    return NULL;
}

/* Process single order and move into user queue */
void *process_order(void *arg){
    Order *order=(Order *)arg;
    /* Look for user we want */
    User *curr=userdb;
    while (curr!=NULL){
        if (order->custID==curr->custID){
            /* Lock user */
            pthread_mutex_lock(&(curr->lock));
            /* Check credit */
            OrderQueue *target;
            if (curr->balance>=order->price){
                curr->balance-=order->price;
                order->credit_left=curr->balance;
                target=curr->completed;
            }else{
                target=curr->failed;
            }
            /* Enqueue to respective user queue */
            pthread_mutex_lock(&(target->mlock));
            enqueue(target,order);
            pthread_mutex_unlock(&(target->mlock));

            pthread_mutex_unlock(&(curr->lock));
            break;
        }
        curr=curr->next;
    }
    sem_post(&finish);
    return NULL;
}

/* Prints the results of the orderdb */
void print_result(User *userdb){
    User *curr=userdb;
    while (curr!=NULL){
        printf("===BEGIN CUSTOMER INFO===\n");
        printf("Customer name: %s\n",curr->name);
        printf("Customer ID number: %d\n",curr->custID);
        printf("Remaining credit balance: %.2f\n",curr->balance);
        printf("### SUCCESSFUL ORDERS ###\n");
        /* Dequeue and print.
         * Destroy the order once we're done because we don't 
         * need it after this.
         */
        Order *order=dequeue(curr->completed);
        while (order!=NULL){
            printf("%s| %.2f| %.2f\n",order->title,order->price,order->credit_left);
            destroy_order(order);
            order=dequeue(curr->completed);
        }
        printf("### REJECTED ORDERS ###\n");
        order=dequeue(curr->failed);
        while (order!=NULL){
            printf("%s| %.2f\n",order->title,order->price);
            destroy_order(order);
            order=dequeue(curr->failed);
        }
        printf("===END CUSTOMER INFO===\n");
        curr=curr->next;
        if (curr!=NULL){
            printf("\n");
        }
    }
}

/* Enqueue order */
void enqueue(OrderQueue *queue, Order *order){
    if (queue->tail==NULL){
        queue->tail=order;
        order->next=order;
    } else {
        order->next=queue->tail->next;
        queue->tail->next=order;
        queue->tail=order;
    }
    queue->size++;
}

/* Dequeue order */
Order* dequeue(OrderQueue *queue){
    if (queue==NULL || queue->tail==NULL){
        return NULL;
    }
    Order *retval=NULL;
    if(queue->tail->next==NULL){
        retval=queue->tail;
    } else {
        retval=queue->tail->next;
        queue->tail->next=queue->tail->next->next;
    }
    queue->size--;
    if (queue->size==0){
        queue->tail=NULL;
    }
    return retval;
}

int main(int argc, char *argv[]) {

    char *userfile; /* file containing user info */

    if (argc < 4) {
        fprintf(stderr, "Incorrect arguments given\n");
        return 1;
    }

    userfile = argv[1];
    userdb=build_userdb(userfile);

    /* Spawn producer */
    pthread_t producer;
    pthread_create(&producer,NULL,build_orderq,argv[2]);
    pthread_join(producer,NULL);

    /* Print out results */
    print_result(userdb);
    
    /* Cleanup */
    destroy_userdb(userdb);
    return 0;
}

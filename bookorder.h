#ifndef BOOKORDER_H
#define BOOKORDER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

typedef struct Order {
    char *title;
    double price;
    int custID;
    char *category;
    double credit_left; /* only use by Order *completed */
    struct Order* next; 
} Order;

/* order queue for each category */
typedef struct OrderQueue {
    Order *tail;
    unsigned int size;
    pthread_mutex_t mlock;
    sem_t slock;
} OrderQueue;

typedef struct User {
    char *name;
    int custID;
    double balance;
    char *address;
    char *state;
    char *zip;
    OrderQueue *completed; // list of completed orders
    OrderQueue *failed; // list of failed orders
    pthread_mutex_t lock; 
    struct User *next;
} User;

User* build_userdb(char *userfile);
void destroy_userdb(User *head);

OrderQueue* create_orderq();
void destroy_orderq(OrderQueue *queue);

void destroy_order(Order *order);

void *build_orderq(void *arg);
void *process_order(void *arg);
void print_result(User *userdb);

void enqueue(OrderQueue *queue, Order *order);
Order* dequeue(OrderQueue *queue);
#endif
